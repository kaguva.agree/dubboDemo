# dubboDemo
项目是简单搭建dubbo分布式微服务，使用zookeeper作为dubbo注册中心，使用maven作为版本管理。简单的学习示例，提供初学者练手学些！

主项目为dubboDemo,使用父子工程，包括的子工程包括dubbo-api,dubbo-consumer,dubbo-provider.
项目的启动顺序是，先安装启动zookeeper,这个是前提条件，因为zookeeper会作为注册中心使用。

项目启动：dubbo-provider---->dubbo-consumer---->dubbo-consumer02---->dubbo-consumer03,
我们在这里使用多个消费者注册使用！，启动之后，我们可以通过dubbo的管理WEB端查看，注册的服务提供者与消费者的情况！

这里的dubbo-admin，直接使用admin的管理版本，启动tomcat即可！

![输入图片说明](https://gitee.com/uploads/images/2018/0306/194516_9015e0a3_1800209.png "屏幕截图.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0306/194601_908dd592_1800209.png "1520336834(1).png")