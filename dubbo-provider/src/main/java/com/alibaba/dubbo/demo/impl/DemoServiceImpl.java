package com.alibaba.dubbo.demo.impl;

import com.alibaba.dubbo.demo.DemoService;

import java.util.ArrayList;
import java.util.List;

public class DemoServiceImpl implements DemoService {
	/* (non-Javadoc)
	 * @see com.alibaba.dubbo.demo.DemoService#getPermissions(java.lang.String, java.lang.Long)
	 */
	public List<String> getPermissions(String consumerID,Long id) {
        List<String> demo = new ArrayList<String>();
        demo.add(String.format(consumerID+" with Permission_%d", id - 1));
        demo.add(String.format(consumerID+" with Permission_%d", id));
        demo.add(String.format(consumerID+" with Permission_%d", id + 1));
        return demo;
    }
}
