package com.alibaba.dubbo.consumer;

import com.alibaba.dubbo.demo.DemoService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Consumer {
    public static void main(String[] args) {
        //测试常规服务
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("consumer.xml");
        context.start();
        System.out.println("consumer02 start");
        DemoService demoService = context.getBean(DemoService.class);
        System.out.println("consumer02");
        System.out.println(demoService.getPermissions("consumer02",1L));
    }
}
